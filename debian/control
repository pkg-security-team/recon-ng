Source: recon-ng
Section: utils
Priority: optional
Maintainer: Debian Security Tools <team+pkg-security@tracker.debian.org>
Uploaders: Marcos Fouces <marcos@debian.org>
Build-Depends:
 debhelper-compat (= 13),
 dh-python,
 python3-all
Rules-Requires-Root: no
Standards-Version: 4.6.0
Homepage: https://github.com/lanmaster53/recon-ng
Vcs-Git: https://salsa.debian.org/pkg-security-team/recon-ng.git
Vcs-Browser: https://salsa.debian.org/pkg-security-team/recon-ng

Package: recon-ng
Architecture: all
Depends:
 libjs-jquery,
 libjs-skeleton,
 node-normalize.css,
 python3,
 python3-dicttoxml,
 python3-dnspython,
 python3-flask,
 python3-lxml,
 python3-requests,
 python3-xlsxwriter,
 python3-yaml,
 python3-mechanize,
 python3-flasgger,
 python3-redis,
 python3-flask-restful,
 python3-rq,
 python3-zombie-imp,
 ${misc:Depends},
 ${python3:Depends}
Description: Web Reconnaissance framework written in Python
 Recon-ng is a full-featured Web Reconnaissance framework written in Python.
 Complete with independent modules, database interaction, built in convenience
 functions, interactive help, and command completion, Recon-ng provides a
 powerful environment in which open source web-based reconnaissance can be
 conducted quickly and thoroughly.
 .
 Recon-ng has a look and feel similar to the Metasploit Framework, reducing the
 learning curve for leveraging the framework. However, it is quite different.
 Recon-ng is not intended to compete with existing frameworks, as it is designed
 exclusively for web-based open source reconnaissance. If you want to exploit,
 use the Metasploit Framework. If you want to Social Engineer, use the Social
 Engineer Toolkit.
